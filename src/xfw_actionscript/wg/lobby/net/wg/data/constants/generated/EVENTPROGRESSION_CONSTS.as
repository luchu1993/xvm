package net.wg.data.constants.generated
{
    public class EVENTPROGRESSION_CONSTS extends Object
    {

        public static const DEFAULT_MODE:String = "front_line_mode";

        public static const FRONT_LINE_MODE:String = "front_line_mode";

        public static const STEEL_HUNTER_MODE:String = "steel_hunter_mode";

        public function EVENTPROGRESSION_CONSTS()
        {
            super();
        }
    }
}
