package net.wg.data.constants.generated
{
    public class BATTLEROYALE_ALIASES extends Object
    {

        public static const VEH_MODULES_CONFIGURATOR_CMP:String = "VehModulesConfiguratorComp";

        public static const LEVEL_UP:String = "battleRoyaleLevelUpView";

        public static const HANGAR_VEH_INFO_VIEW:String = "HangarVehInfoView";

        public static const EVENT_PROGRESSION_VIEW:String = "eventProgressionView";

        public static const TECH_PARAMETERS_COMPONENT:String = "techParametersComponent";

        public static const COMMANDER_COMPONENT:String = "commanderComponennt";

        public static const BOTTOM_PANEL_COMPONENT:String = "bottomPanelComponent";

        public static const VEHICLE_WEAK_ZONES_CMP:String = "weakZonesComponent";

        public static const BATTLE_ROYALE_SUMMARY_RESULTS_CMP:String = "BattleRoyaleSummaryResultsCmp";

        public static const BATTLE_ROYALE_SCORE_RESULTS_CMP:String = "BattleRoyaleScoreResultsCmp";

        public static const LEVEL_UP_UI:String = "battleRoyaleLevelUpView.swf";

        public function BATTLEROYALE_ALIASES()
        {
            super();
        }
    }
}
