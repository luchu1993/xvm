package net.wg.infrastructure.base.meta
{
    import flash.events.IEventDispatcher;

    public interface IDemountKitInfoMeta extends IEventDispatcher
    {

        function onCancelClickS() : void;

        function as_setDemountKitInfo(param1:Object) : void;
    }
}
