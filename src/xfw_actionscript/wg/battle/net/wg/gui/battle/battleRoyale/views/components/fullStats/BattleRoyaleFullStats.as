package net.wg.gui.battle.battleRoyale.views.components.fullStats
{
    import net.wg.infrastructure.base.meta.impl.BattleRoyaleFullStatsMeta;
    import net.wg.infrastructure.base.meta.IBattleRoyaleFullStatsMeta;
    import flash.display.Sprite;
    import net.wg.gui.battle.battleRoyale.views.components.EventViewHeader;
    import net.wg.gui.battle.battleRoyale.views.components.fullStats.nationsVehiclesCounter.BattleRoyaleNationsVehiclesCounter;
    import net.wg.data.constants.InvalidationType;
    import flash.events.Event;
    import net.wg.gui.battle.battleRoyale.data.BattleRoyaleFullStatsVO;
    import scaleform.clik.data.DataProvider;
    import net.wg.gui.battle.battleRoyale.views.components.fullStats.nationsVehiclesCounter.data.BattleRoyaleNationsVehiclesCounterVO;
    import net.wg.gui.battle.battleRoyale.BattleRoyalePage;

    public class BattleRoyaleFullStats extends BattleRoyaleFullStatsMeta implements IBattleRoyaleFullStatsMeta
    {

        private static const SCORE_X:uint = 48;

        private static const SCORE_X_BIG:uint = 87;

        private static const NATIONS_VEHICLES_COUNTER_BOTTOM_OFFSET:int = 8;

        private static const NATIONS_VEHICLES_COUNTER_BOTTOM_OFFSET_BIG:int = 23;

        private static const MINIMAP_ITEMS_BOTTOM_OFFSET:int = 291;

        private static const MINIMAP_ITEMS_BOTTOM_OFFSET_BIG:int = 453;

        private static const CONTENT_SMALL_SIZE:int = 507;

        private static const CONTENT_BIG_SIZE:int = 725;

        public var bg:Sprite = null;

        public var header:EventViewHeader = null;

        public var minimapItems:MinimapItemsInfo = null;

        public var score:ScoreBlock = null;

        public var nationsVehiclesCounter:BattleRoyaleNationsVehiclesCounter = null;

        private var _isSmallScreenSize:Boolean = false;

        public function BattleRoyaleFullStats()
        {
            super();
        }

        override protected function draw() : void
        {
            super.draw();
            if(isInvalid(InvalidationType.SIZE))
            {
                this.layoutElements();
            }
        }

        override protected function initialize() : void
        {
            super.initialize();
            this.nationsVehiclesCounter.addEventListener(Event.RESIZE,this.onResizeHandler);
            this.minimapItems.addEventListener(Event.RESIZE,this.onResizeHandler);
        }

        override protected function onDispose() : void
        {
            this.nationsVehiclesCounter.removeEventListener(Event.RESIZE,this.onResizeHandler);
            this.nationsVehiclesCounter.dispose();
            this.nationsVehiclesCounter = null;
            this.minimapItems.removeEventListener(Event.RESIZE,this.onResizeHandler);
            this.minimapItems.dispose();
            this.minimapItems = null;
            this.score.dispose();
            this.score = null;
            this.header.dispose();
            this.header = null;
            this.bg = null;
            super.onDispose();
        }

        override protected function setData(param1:BattleRoyaleFullStatsVO) : void
        {
            this.header.setData(param1.header);
            this.score.setData(param1);
            this.minimapItems.dataProvider = new DataProvider(param1.minimapItems);
        }

        override protected function updateNationsVehiclesCounter(param1:BattleRoyaleNationsVehiclesCounterVO) : void
        {
            this.nationsVehiclesCounter.setData(param1);
            invalidateSize();
        }

        public function as_updateScore(param1:int, param2:int, param3:String) : void
        {
            this.score.update(param1,param2,param3);
        }

        public function updateStage(param1:Number, param2:Number) : void
        {
            this.header.updateStage(param1,param2);
            this.bg.width = param1;
            this.bg.height = param2;
            this.bg.x = -param1 >> 1;
            this._isSmallScreenSize = param2 <= BattleRoyalePage.SCREEN_SMALL_HEIGHT || param1 <= BattleRoyalePage.SCREEN_SMALL_WIDTH;
            this.score.useSmallLayout = this._isSmallScreenSize;
            this.minimapItems.useSmallLayout = this._isSmallScreenSize;
            this.nationsVehiclesCounter.useSmallLayout = this._isSmallScreenSize;
            var _loc3_:Number = this.header.getContentHeight();
            var _loc4_:int = this._isSmallScreenSize?CONTENT_SMALL_SIZE:CONTENT_BIG_SIZE;
            var _loc5_:* = param2 + _loc3_ - _loc4_ >> 1;
            this.score.x = this._isSmallScreenSize?SCORE_X:SCORE_X_BIG;
            this.score.y = _loc5_;
            invalidateSize();
        }

        private function layoutElements() : void
        {
            this.nationsVehiclesCounter.x = this.score.x + this.score.width - this.nationsVehiclesCounter.width | 0;
            this.nationsVehiclesCounter.y = this.score.y + this.score.height + (this._isSmallScreenSize?NATIONS_VEHICLES_COUNTER_BOTTOM_OFFSET:NATIONS_VEHICLES_COUNTER_BOTTOM_OFFSET_BIG) | 0;
            this.minimapItems.x = this.score.x;
            this.minimapItems.y = this.score.y + (this._isSmallScreenSize?MINIMAP_ITEMS_BOTTOM_OFFSET:MINIMAP_ITEMS_BOTTOM_OFFSET_BIG);
        }

        override public function set visible(param1:Boolean) : void
        {
            if(param1 != visible)
            {
                dispatchEvent(new Event(param1?Event.OPEN:Event.CLOSE));
            }
            super.visible = param1;
        }

        private function onResizeHandler(param1:Event) : void
        {
            invalidateSize();
        }
    }
}
