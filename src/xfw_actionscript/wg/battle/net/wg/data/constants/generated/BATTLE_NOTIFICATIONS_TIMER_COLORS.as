package net.wg.data.constants.generated
{
    public class BATTLE_NOTIFICATIONS_TIMER_COLORS extends Object
    {

        public static const ORANGE:String = "orange";

        public static const GREEN:String = "green";

        public static const RED:String = "red";

        public function BATTLE_NOTIFICATIONS_TIMER_COLORS()
        {
            super();
        }
    }
}
