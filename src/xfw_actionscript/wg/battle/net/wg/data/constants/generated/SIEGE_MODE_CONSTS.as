package net.wg.data.constants.generated
{
    public class SIEGE_MODE_CONSTS extends Object
    {

        public static const HYDRAULIC_CHASSIS_TYPE:String = "hydraulicChassisType";

        public static const TURBOSHAFT_ENGINE_TYPE:String = "turboshaftEngineType";

        public static const SIEGE_MODE_TYPES:Array = [HYDRAULIC_CHASSIS_TYPE,TURBOSHAFT_ENGINE_TYPE];

        public function SIEGE_MODE_CONSTS()
        {
            super();
        }
    }
}
