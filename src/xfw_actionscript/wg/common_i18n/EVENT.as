package
{
    public class EVENT extends Object
    {

        public static const PUNISHMENTWINDOW_REASON_EVENT_DESERTER:String = "#event:punishmentWindow/reason/event_deserter";

        public static const PUNISHMENTWINDOW_REASON_EVENT_AFK:String = "#event:punishmentWindow/reason/event_afk";

        public function EVENT()
        {
            super();
        }
    }
}
