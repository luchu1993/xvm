package net.wg.data.constants.generated
{
    public class BATTLE_MARKER_STATES extends Object
    {

        public static const STUN_STATE:int = 0;

        public static const INSPIRING_STATE:int = 1;

        public static const INSPIRED_STATE:int = 2;

        public static const ENGINEER_STATE:int = 3;

        public static const HEALING_STATE:int = 4;

        public static const BERSERKER_STATE:int = 5;

        public static const REPAIRING_STATE:int = 6;

        public function BATTLE_MARKER_STATES()
        {
            super();
        }
    }
}
